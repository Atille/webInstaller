<?php

class Flow {

	// Set the archives to download.
	var $archives = [
		1 => 'wordpress.zip',
	];

	public static function filesExist($archives) {

		# Check if archives are already downloaded
		foreach($archives as $archive) {
			if(file_exists("zips/$archive")) {
				return true;

			# If archives doesn't exist, download it.
			} else {
				Flow::fetchArchives();
			}
		}
	}

	public static function fetchArchives($archives) {

		# If the zips/ doesn't exists, create it.
		if(!file_exists("zips")) {
			mkdir("zips/");
		}

		# Needs to be corrected.
		foreach($archives as $archive) {
			shell_exec("wget https://wordpress.org/latest.zip -O zips");
		}
	}
}