<?php

class Start {

	public static function launch($debug = NULL, $arguments) {
		# Add a first debug step if $debug is set.
		if($debug === 1) {
			error_reporting(E_ALL);
			ini_set("display_errors", 1);
		}

		if(Analysis::areOptionsSet($arguments)) {
			if(Flow::filesExist()) {

			} else {
				return false;
			}
		}
	}

}

require "analysis.class.php";
Start::launch($debug = NULL, $arguments = $argv);
?>
